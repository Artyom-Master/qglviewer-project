#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class Viewer;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private Q_SLOTS:
    void quit();
    void saveParaboloid();
    void saveRandom();
    void illustrateFigure();

private:
    Ui::MainWindow *ui;
    Viewer *viewer;
};
#endif // MAINWINDOW_H
