#include "mainwindow.h"
#include "viewer.h"
#include "ui_mainwindow.h"
#include <QFileDialog>
#include <QTextStream>
#include <QRandomGenerator>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)    
{
    ui->setupUi(this);
    connect(ui->quitButton, &QPushButton::clicked, this, &MainWindow::quit);
    connect(ui->saveRandomButton, &QPushButton::clicked, this, &MainWindow::saveRandom);
    connect(ui->saveParaboloidButton, &QPushButton::clicked, this, &MainWindow::saveParaboloid);
    connect(ui->openFigureFromFileButton, &QPushButton::clicked, this, &MainWindow::illustrateFigure);
}

Viewer *viewer;

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::quit()
{
    this->close();
}

void MainWindow::saveRandom ()
{
    QString filename = QFileDialog::getSaveFileName(this, "Save file", "Random", "Text files (*.txt)");
    QFile file(filename);
    if(!file.open(QFile::WriteOnly | QFile::Text)) return;
    QTextStream w(&file);
    QRandomGenerator rd;
    float x, y, z;
    for(int i = 0; i < 1000000; i++)
    {
        if(i == 500000) rd.seed();
        x = rd.bounded(-100, 100)/13;
        y = rd.bounded(-100, 100)/23;
        z = rd.bounded(-100, 100)/41;
        w << x << " " << y << " " << z << "\n";
    }
    file.close();
}

void MainWindow::saveParaboloid()
{
    QString filename = QFileDialog::getSaveFileName(this, "Save file", "Paraboloid", "Text files (*.txt)");
    QFile file(filename);
    if(!file.open(QFile::WriteOnly | QFile::Text)) return;
    QTextStream w(&file);
    const float nbSteps = 200;
    for(float i = 0; i < 50; i += 0.01)
    {
        const float r = i/2;
        const float y = r*r;
        for(int j = 0; j < nbSteps; j++)
        {
             const float ratio = 2*3.14*j / nbSteps;
             const float c = cos(ratio);
             const float s = sin(ratio);
             w << r*c << " " << y << " " << r*s << "\n";
        }
    }
    file.close();
}

void MainWindow::illustrateFigure()
{
    QString filename = QFileDialog::getOpenFileName(this,"Open file", "", "Text files (*.txt)");
    QFile file(filename);
    if(!file.open(QFile::ReadOnly | QFile::Text)) return;
    QString pointsInFile = file.readAll();
    viewer = new Viewer(&pointsInFile);
    viewer->show();
    file.close();
}

