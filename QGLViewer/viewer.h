#ifndef VIEWER_H
#define VIEWER_H

#include <QGLViewer/qglviewer.h>
#include <QOpenGLWidget>
#include <QFile>

class Viewer : public QGLViewer {
    Q_OBJECT
private:
    QString *pointsCloud;
public:
  Viewer(QString *points)
  {
      pointsCloud = points;
      setAttribute(Qt::WA_DeleteOnClose);
  };
  virtual void draw();
  virtual void init();
};

#endif // VIEWER_H
