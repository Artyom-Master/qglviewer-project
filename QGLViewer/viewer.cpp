#include <viewer.h>

void Viewer::init() {
  if(!restoreStateFromFile())
      showEntireScene();
}

void Viewer::draw()
{
    QString line;
    QStringList points = pointsCloud->split(QRegExp("[\n]"), Qt::SkipEmptyParts);
    glBegin(GL_POINTS);
    Q_FOREACH(line, points)
    {
        glColor3f(1, 0, 0);
        glVertex3f(line.section(" ", 0, 0).toFloat(), line.section(" ", 1, 1).toFloat(), line.section(" ", 2, 2).toFloat());
    }
    glEnd();
}
